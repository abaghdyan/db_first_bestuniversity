﻿using LINQBestUniversity.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQBestUniversity
{
    static class MicManager
    {
        public static void AddRandomStudents(int count)
        {
            using (StudentEntities studentEntity = new StudentEntities())
            {
                Random rnd = new Random();
                for (int i = 0; i < count; i++)
                {
                    studentEntity.Students.Add(new Students()
                    {
                        Name = "A" + i,
                        Surname = "A" + i + "yan",
                        Mark = (byte)rnd.Next(20),
                        Age = (byte)rnd.Next(18, 45),
                        University = ((University)rnd.Next(4)).ToString(),
                        Gender = ((Gender)rnd.Next(2)).ToString()
                    });
                }
                studentEntity.SaveChanges();
            }
        }
        public static void RemoveAllStudents()
        {
            using (StudentEntities studentEntity = new StudentEntities())
            {
                studentEntity.Students.RemoveRange(studentEntity.Students);
                studentEntity.SaveChanges();
            }
        }
    }
}
