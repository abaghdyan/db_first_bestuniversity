﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQBestUniversity
{
    class Program
    {
        static void Main(string[] args)
        {
            MicManager.RemoveAllStudents();
            MicManager.AddRandomStudents(20);
            using (StudentEntities studentEntities = new StudentEntities())
            {
                var bestUniversity = studentEntities.Students
                    .GroupBy(p => p.University)
                    .ToDictionary(p => p.Key, p => p.OrderByDescending(p1 => p1.Mark).Take(3).Average(p1 => p1.Mark))
                    .OrderByDescending(p => p.Value)
                    .Take(1);

                foreach (var item in bestUniversity)
                {
                    Console.WriteLine($"Best university: {item.Key} with mark {item.Value}");
                }
            }
            Console.ReadLine();
        }
    }
}
